#!/bin/bash

#1.create directory for holding the graphs and the contigs
mkdir ../results/benchmark_ecoli_graphs
mkdir ../results/benchmark_ecoli_contigs

#1. get a list of strains
files=$(cat ../results/benchmark_strains.csv | sed 's/"//g')

#2. Loop thru the unicycler directories and copy the graphs
for genome in $files
do
cp ../results/unicycler_assemblies/${genome}/assembly.gfa ../results/benchmark_ecoli_graphs/${genome}.gfa
cp ../results/unicycler_assemblies/${genome}/assembly.fasta ../results/benchmark_ecoli_contigs/${genome}.fasta
done
