#!/bin/bash

#1. create directory for storing reference genomes 
mkdir ../data/reference_genomes

#2. create directory for story compressed reference genomes
mkdir ../data/compressed

#3. activate environment
conda activate ncbi_download_mmbioit

cd ../data

#4. Extract ftp address to download the data from the file paganini_2021_benchmark_strains_metadata.csv
tail -n+2 paganini_2021_benchmark_strains_metadata.csv | cut -f 13 -d , | sed 's/"//g' > ftp_temp.txt
awk '{FS="/";print $0"/"$10"_genomic.fna.gz";}' ftp_temp.txt > ftp.txt
links=$(cat ftp.txt)

#5. Download data
for strains in $links
do
wget ${strains} -P compressed/
done

#6. Uncompress genomes
cd compressed/
genomes=$(ls *gz | sed 's/.gz//g')
for things in $genomes
do
gunzip -c ${things}.gz > ../reference_genomes/${things}
done
