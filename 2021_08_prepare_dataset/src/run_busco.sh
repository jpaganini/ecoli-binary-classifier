#!/bin/bash

#0. Activate the conda environment
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate busco

#1. Make directory for holing the busco results
mkdir ../results/busco_output

#2. Make a list of the files
files=$(cat ../results/benchmark_strains.csv | sed 's/"//g')

#4. Loop thru strains to get the output
for strain in $files
do
busco -m geno -i ../../2021_08_16_benchmarking_ec/data/reference_genomes/${strain}_genomic.fna -o ${strain} --out_path ../results/busco_output  -c 8 -l enterobacterales_odb10
done
