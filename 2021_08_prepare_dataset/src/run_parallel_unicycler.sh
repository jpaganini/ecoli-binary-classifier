#!/bin/bash

#1. get command line optoins
while getopts :s:i:o: flag; do
        case $flag in
                s) strain_file=$OPTARG;;  #file that contains a list of strain names
                i) trimmed_reads=$OPTARG;; #directory that contains trimmed short-reads (same names as strains)
        o) output_directory=$OPTARG;; #directory that will contain the final unicycler assemblies.
        esac
done

#2. Clean paths given by user
trimmed_reads=$(echo ${trimmed_reads} | sed 's#/*$##g')
output_directory=$(echo ${output_directory} | sed 's#/*$##g')

#4. get a list of files
files=$(cat ${strain_file} | sed 's/"//g') 

#make directory to hold results
mkdir -p ${output_directory}

#create folder for temporary slurm scripts
mkdir unicycler_slurm_jobs

#create slurm scripts

for strains in $files
do
echo "#! /bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh #this has to be modified
conda activate unicycler
cd ${trimmed_reads}
unicycler -1 ${strains}_R1.fq -2 ${strains}_R2.fq -o ${output_directory}/${strains} --threads 8 --min_fasta_length 1000" > unicycler_slurm_jobs/${strains}.sh
done

#Run the scripts
cd unicycler_slurm_jobs
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=8:00:00 --mem=10G ${slurm}
done

