#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate quast_mmbioit

#mkdir for the general results
mkdir -p ../results/quast_output/self_alignments_gfa_1kb
mkdir -p ../results/quast_output/self_alignments_gfa_500

##----------run quast for self-alignment --------------------#

#move to the folder that holds the graphs
cd ../plasmidEC/ecoli_nodes_1kb/
files=$(ls *fasta | sed 's/.fasta//g')

#run quast, the target the simulated contigs of the strains against the complete genome of the strains, to later obtain the number of contigs that align to each plasmid.
for strains in $files
do
quast -o ../../results/quast_output/self_alignments_gfa_1kb/${strains} -r ../../../2021_08_prepare_dataset/data/reference_genomes/${strains}_genomic.fna -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${strains}.fasta
done

#run quast for 500bp
cd ../ecoli_nodes_500bp
for strains in $files
do
quast -o ../../results/quast_output/self_alignments_gfa_500/${strains} -r ../../../2021_08_prepare_dataset/data/reference_genomes/${strains}_genomic.fna -m 500 -t 8 -i 300 --no-snps --ambiguity-usage all ${strains}.fasta
done



