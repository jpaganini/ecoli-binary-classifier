#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 10:21:35 2020

@author: jpaganini
"""
import sys
import os
import glob
import fastaparser
import re
from collections import defaultdict




#1. Create a dictionary with the lengths of each of the contigs included in the predictions

bin_length_dict={}

def create_bin_length_dict(strain):
    #move to the strain directory and list all bins for that strains/ # this will have to be modified 
    # for every tool
    global bin_length_dict
    global predictions_directory
    global total_bin_lengths
    total_bin_lengths={}
    bin_length_dict={}
    os.chdir(predictions_directory)
    bins=glob.glob(strain+'*fasta')#this will have to change for every software
    for prediction in bins:
        prediction_modified=prediction.replace('.fasta','')
        with open(prediction) as fasta_prediction:
            prediction_parser = fastaparser.Reader(fasta_prediction, parse_method='quick')
            for seq in prediction_parser:
                #the contig name might have to be change for every software
                contig_name=seq.header.replace('>','')
                contig_name=contig_name.replace(':','_')
                contig_length=len(seq.sequence)
                bin_length_dict[contig_name]=contig_length
                if prediction_modified in total_bin_lengths:
                    total_bin_lengths[prediction_modified]+=int(contig_length)
                else:
                    total_bin_lengths[prediction_modified]=int(contig_length)
                             
    os.chdir(wd)

def bin_status(strain):
    global bin_length_dict
    global total_bin_lengths
    os.chdir(alignment_directory)
    try:
        os.chdir(strain)
        bins=glob.glob('GCA*') #this might have to be changed for the different softwares

        #this loop will run for every bin in the strain (which is defined outside the function)
        for prediction in bins:
            #change format found on quast output
            new_prediction=prediction.replace('.','-') 
            
###---------Create dictionaries to temporary hold results while reading files-------###
            ##for correct alignments
            correct_lengths={} #this is a dictionary {reference_id:length_of_contig}. It will hold the total lenght of correct aligments
            correct_contig_count={} #dictionary to hold how many contigs aligned to the reference replicon
            correct_length_percentages={} #dictionary to hold the percent of contig that is correctly algined. (coverage%) 
            correct_count_percentages={}
            ##or ambiguous alignments
            ambiguous_references=set() #this is a set for holding the references names of the ambiguous alignments
   
###---------Start counters for different parameters------###
            ##count the ambiguous alignments
            ambiguous_count=0
            ##track the total lenght of unaligned contigs
            unaligned_length=0 
            ##track the number of contigs unaligned per prediction
            unaligned_count=0
            ##track the number of contigs that were partially aligned (less than 90% coverage)
            low_qcov=0
            
###---------Start dictionary for tracking the min and max position of alignments, for detecting overlappings------###
            min_alignnment_pos_dict=defaultdict(list)
            max_alignnment_pos_dict=defaultdict(list)
            ##dictionary for holidng the number of bp that are overlapping in each replicon
            overlapping={}

###---------START ANALYZING THE QUAST ALIGNMENT FILE  --------------------------##          
            try:
                #open alignment report from QUAST
                with open(prediction+'/contigs_reports/all_alignments_'+new_prediction+'.tsv') as contig_alignment:
                    #store all lines in a variable call all_lines
                    all_lines=contig_alignment.readlines()[1:]
                    #start a counter for analyzing the number of the line being analyzed/
                    i=0
                    #create a variable for storing lines that do not contain classification of alignments (correct, ambiguous, etc). 
                    information_lines=[] #This variable will be resetted to empty every time a classification line is found.
                    #Loop thru the lines in the QUAST file
                    for line in all_lines:
                        line=line.rstrip()
###------------------------------1. PROCESS DATA FOR CONTIGS THAT ARE PARTIALLY ALIGNED. 
                        if 'correct_unaligned' in line: #these are contigs that have a region aligned and another region unaligned.
                            information=information_lines[0].split('\t') #the information about alignment is on the previous line.
                            reference_name=information[4] #this is the replicon reference id
                            alignment_length=abs(int(information[3])-int(information[2]))+1 #INITIAL LENGTH OF THE ALIGNMENT
                            indels=abs(alignment_length-(abs(int(information[0])-int(information[1]))+1)) #CHECK THE INDELS
                            alignment_length=alignment_length-indels #REAL LENGTH OF ALIGNMENT
                            contig_name=information[5] #NAME OF THE CONTIG
                                            
               #If more than 90% of the contig length is aligned, we will consider this as a correct alignment 
                            if int(alignment_length) >= int(bin_length_dict[contig_name])*0.9:
                                #store the name of the replicon to which is aligned. why?
                                #store the percentage of the contig that is aligned.
                                alignment_fraction=round(int(alignment_length)/int(bin_length_dict[contig_name]),2)
                                #gather alignment ranges for later evaluation of overlapping positions.
                                #Overlapping analysis has to be performed after reading the complete file.
                                reference_positions=[int(information[0]),int(information[1])]
                                start=str(min(reference_positions))
                                end=str(max(reference_positions))
                                #store data in the dictionary.
                                min_alignnment_pos_dict[reference_name].append(start)
                                max_alignnment_pos_dict[reference_name].append(end)
                                 
                                #Write the alignment length and fraction of the contig
                                with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                    reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
            
                                #add the length of the alignment to the correct_lengths dictionary. Reference_replicon:total_alignment_length    
                                if reference_name in correct_lengths:
                                    correct_lengths[reference_name]=int(correct_lengths[reference_name])+int(alignment_length)    
                                else:
                                    correct_lengths[reference_name]=int(alignment_length)
                                      
                                #Update the count of correctly aligned contigs.
                                if reference_name in correct_contig_count:
                                    correct_contig_count[reference_name]=float(correct_contig_count[reference_name])+1    
                                else:
                                    correct_contig_count[reference_name]=1
                                    
                          #If less than 90% of the contig is aligned, we will consider this as unaligned contig            
                            else:
                                #get the alignment fraction
                                alignment_fraction=round(int(alignment_length)/int(bin_length_dict[contig_name]),2)
                                #update the total unaligned length in the prediction
                                unaligned_length+=int(alignment_length)
                                #update the count of unaligned contigs int he prediction
                                unaligned_count+=1
                                #update the count of contigs with less than 90% alignment
                                low_qcov+=1
                                with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                    reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
                                with open(output_directory+'/unaligned_contigs.csv', 'a+') as unaligned_file:
                                    unaligned_file.write(strain+','+contig_name+','+str(bin_length_dict[contig_name])+','+'\n')
                                
                            #move to the next line for analysis 
                            i+=1
                            #clean the accumulated lines.
                            information_lines=[]

###------------------------------2. PROCESS DATA FOR CONTIGS THAT ARE UNALIGNED. 
                        elif 'unaligned' in line:  #Consider unaligned regions
                            unaligned_length+=bin_length_dict[contig_name]
                            unaligned_count+=1
                                
                            #we will gather the information of the unaligned contig.
                            contig_name=information[1]
                            with open(output_directory+'/unaligned_contigs.csv', 'a+') as unaligned_file:
                                unaligned_file.write(strain+','+contig_name+','+str(bin_length_dict[contig_name])+','+'\n')
                            
                            #clean the accumulated lines.
                            information_lines=[]
                            #move to the next line for analysis 
                            i+=1

###------------------------------3. PROCESS DATA FOR CONTIGS THAT ARE CORRECTLY ALIGNED, ACCORDING TO QUAST.                         
                        elif 'correct' in line:
                            
                            ###---3.1 CORRECT, BUT WITH MINOR ASSEMBLY ISSUES OR ALIGNMENT ISSUES
                            
                            if any('indel' in words for words in information_lines) or any('local misassembly' in words for words in information_lines) or any('scaffold gap' in words for words in information_lines) or any('linear representation of circular genome' in words for words in information_lines):
                                #If we found any of this words around previous to the Correct match, it means that there was some minor problem with the Assembly of that contig.#See QUAST MANUAL. 
                                #As a result, alignment information for a contig-reference_replicon will be splitted into multiple lines. So...
                                                           
                                #we will create a dictionary that will temporarly accumulate the alignment lengths of the different secitions of the contig.
                                  temporary_alignment_dict={} #{reference_replicon:total_length_of_alignment}
                                  #GET THE LENGT OF THE LINES THAT HAVE BEEN ACCCUMULATED UP TO THAT POINT
                                  total_information_lines=len(information_lines)
                                  #we will create a dictionary that will contain the alignments ranges temporarily. For analyzing overallping
                                  temporary_min_alignnment_pos_dict=defaultdict(list) #{reference replicon:min-alignment-position}
                                  temporary_max_alignnment_pos_dict=defaultdict(list) #{reference replicon:max-alignment-position}
                                  
                                  #get a counter to loop thru the temporary lines
                                  j=0
                                  #loop thru this temporary lines
                                  while j < total_information_lines:
                                          alignment_information=information_lines[j].split('\t')
                                          if any('False' in words for words in alignment_information): #skipping false alignments
                                              j+=1
                                              
                                          else:
                                             
                                              try: #if line contains information about the alignment, process it, otherwise skip it.
                                                  #gather information from the contig
                                                  reference_name=alignment_information[4]
                                                  contig_name=alignment_information[5]
                                                  alignment_length=abs(int(alignment_information[3])-int(alignment_information[2]))+1 #LENGTH OF THE ALIGNMENT
                                                  indels=abs(alignment_length-(abs(int(alignment_information[0])-int(alignment_information[1]))+1))
                                                  alignment_length=alignment_length-indels #real length

                                                  #save alignment ranges for later analysis of overlapping regions.
                                                  reference_positions=[int(alignment_information[0]),int(alignment_information[1])]
                                                  start=str(min(reference_positions))
                                                  end=str(max(reference_positions))
                                                  temporary_min_alignnment_pos_dict[reference_name].append(start)
                                                  temporary_max_alignnment_pos_dict[reference_name].append(end) 
                                                  
                                                  #add the length of alignment ot the alignment length dict.
                                                  if reference_name in temporary_alignment_dict:
                                                      temporary_alignment_dict[reference_name]=int(temporary_alignment_dict[reference_name])+int(alignment_length) 
                                                  else:
                                                      temporary_alignment_dict[reference_name]=int(alignment_length)
            
                                                  #go to the next accumulated line
                                                  j+=1
                                                  
                                              except: #if the line does not have information about contig alignment, skip it.
                                                  j+=1
                                
                                  #now we will check if the accumulated lengths of alignments correspond to 90% of the contig length        
                                  for reference_name in temporary_alignment_dict:
                                      if int(temporary_alignment_dict[reference_name]) >= int(bin_length_dict[contig_name])*0.9:
                                          alignment_fraction=round(int(temporary_alignment_dict[reference_name])/int(bin_length_dict[contig_name]),2)
                                          #if this true, we will count the alignment as correct and we will add it to the correct_lenths dictionary
                                          
                                          #now we will merge the dictionary of temporary alignment positions
                                          entry_numbers=len(temporary_min_alignnment_pos_dict[reference_name])
                                          k=0
                                          while k < entry_numbers:
                                              start=temporary_min_alignnment_pos_dict[reference_name][k]
                                              end=temporary_max_alignnment_pos_dict[reference_name][k]
                                              min_alignnment_pos_dict[reference_name].append(start)
                                              max_alignnment_pos_dict[reference_name].append(end)
                                              k+=1
                                                                                          
                                          if reference_name in correct_lengths:
                                              correct_lengths[reference_name]=int(correct_lengths[reference_name])+int(temporary_alignment_dict[reference_name]) 
                                          else:
                                              correct_lengths[reference_name]=int(temporary_alignment_dict[reference_name])
                                          
                                          if reference_name in correct_contig_count:
                                              correct_contig_count[reference_name]=float(correct_contig_count[reference_name])+1    
                                          else:
                                              correct_contig_count[reference_name]=1
                                                  
                                        #we will andd the name of the contig and the name of the refrence to the reference_file. This will be later used to study atb-R location
                                          with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                              reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
                                              
                                    #If less than 90% of the contig is aligned, we will consider this as unaligned contig      
                                      else:
                                          alignment_fraction=round(int(temporary_alignment_dict[reference_name])/int(bin_length_dict[contig_name]),2)
                                          unaligned_length+=int(alignment_length)
                                          unaligned_count+=1
                                          low_qcov+=1
                                          with open(output_directory+'/unaligned_contigs.csv', 'a+') as unaligned_file:
                                              unaligned_file.write(strain+','+contig_name+','+str(bin_length_dict[contig_name])+','+'\n')
                                          with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                              reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
                                  
                                #after analyzing the data accumulated until we foound the 'correct' line,we will clean the saved data and then move to the next line.
                                  information_lines=[]
                                  i+=1
   
                            ###---3.2 CORRECT, NO ISSUES.
                            else:
                                #extract the information. It should it be on the line previous to the last.
                                information=all_lines[i-1].split('\t')
                                reference_name=information[4] #this is the replicon reference id
                                alignment_length=abs(int(information[3])-int(information[2]))+1 #LENGTH OF THE ALIGNMENT
                                indels=abs(alignment_length-(abs(int(information[0])-int(information[1]))+1))
                                alignment_length=alignment_length-indels #REAL LENGTH OF THE ALIGNMENT
                                contig_name=information[5]
                          
                                #check if more than 90% of the contig aligns.
                                if int(alignment_length) >= int(bin_length_dict[contig_name])*0.9:
                                    alignment_fraction=round(int(alignment_length)/int(bin_length_dict[contig_name]),2)                                   
                                    #gather alignment ranges for later evaluation of overlapping positions
                                    reference_positions=[int(information[0]),int(information[1])]
                                    start=str(min(reference_positions))
                                    end=str(max(reference_positions))
                                    min_alignnment_pos_dict[reference_name].append(start)
                                    max_alignnment_pos_dict[reference_name].append(end) 
      
                    #Add the length of the alignment to the dictionary correct_lengths. This will be use to calculate the purity percentage of the bin
                                    if reference_name in correct_lengths:
                                        correct_lengths[reference_name]=int(correct_lengths[reference_name])+int(alignment_length)    
                                    else:
                                        correct_lengths[reference_name]=int(alignment_length)
                                        
                  #Count the amount of correctly aligned contigs for each reference. This will be use to calculate the purity percentage of the bin
                                    if reference_name in correct_contig_count:
                                        correct_contig_count[reference_name]=float(correct_contig_count[reference_name])+1    
                                    else:
                                        correct_contig_count[reference_name]=1
                                        
                                    with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                        reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
                                    
                                    i+=1
                                    information_lines=[]
                                else: #If less than 90% aligns, consider it unaligned.
                                    unaligned_length+=int(alignment_length)
                                    unaligned_count+=1
                                    low_qcov+=1
                                    with open(output_directory+'/unaligned_contigs.csv', 'a+') as unaligned_file:
                                        unaligned_file.write(strain+','+contig_name+','+str(bin_length_dict[contig_name])+','+'\n')
                                    with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                        reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
                                    i+=1
                                    information_lines=[]
                                    
                                    
###------------------------------4. PROCESS DATA FOR CONTIGS THAT ARE AMBIGUOUSLY ALIGNED, ACCORDING TO QUAST.

                        #AMBIGUOUS REGIONS WILL BE CONSIDERED AS CORRECT ALIGNMENTS. 
                        #BUT ALSO RECORDED IN SEPARATED FILE (ambiguous_refrences file). IN CASE WE WANT TO REMOVE THEM AFTER.
                        
#IN CASES OF AMBIGUOUS ALIGNMENTS, MULTIPLE LINES WILL BELONG TO THE SAME CONTIG, ALIGNING TO EITHER DIFFERENT REGIONS OF THE SAME REPLICON OR TO MULTIPLE REPLICONS                   
                        elif 'ambiguous' in line: 
                            ambiguous_count+=1
                            for lines in information_lines: #LOOP THRU ALL LINES ACCUMULATED UNTIL 'AMBIGUOUS' CLASSIFICATION HAS BEEN FOUND.
                                #this section of the code will gather the information on the refrence replicon and of the contig name.
                                ambiguous_information=lines.split('\t')
                                ambiguous_reference_name=ambiguous_information[4]
                                ambiguous_references.add(ambiguous_reference_name)
                                ambiguous_alignment_length=abs(int(ambiguous_information[3])-int(ambiguous_information[2]))+1 #LENGTH OF THE ALIGNMENT
                                indels=abs(ambiguous_alignment_length-(abs(int(ambiguous_information[0])-int(ambiguous_information[1]))+1))
                                ambiguous_alignment_length=ambiguous_alignment_length-indels #REAL LENGHT OF ALIGNMENT
                                contig_name=ambiguous_information[5]
                             
                                #Check if more than 90% of the contig correctly aligns. And record info.
                                if int(ambiguous_alignment_length) >= int(bin_length_dict[contig_name])*0.9:
                                      alignment_fraction=round(int(ambiguous_alignment_length)/int(bin_length_dict[contig_name]),2)                                      
                                      #gather alignment ranges for later evaluation of overlapping positions
                                      reference_positions=[int(ambiguous_information[0]),int(ambiguous_information[1])]
                                      start=str(min(reference_positions))
                                      end=str(max(reference_positions))
                                      min_alignnment_pos_dict[ambiguous_reference_name].append(start)
                                      max_alignnment_pos_dict[ambiguous_reference_name].append(end)

                                      if ambiguous_reference_name in correct_lengths:                            
                                          correct_lengths[ambiguous_reference_name]=int(correct_lengths[ambiguous_reference_name])+int(ambiguous_alignment_length)
                                      else:
                                          correct_lengths[ambiguous_reference_name]=int(ambiguous_alignment_length)
                                          
                                    #Count the amount of correctly aligned contigs for each reference. This will be use to calculate the precision of the bin
                                      if ambiguous_reference_name in correct_contig_count:                                     
                                          correct_contig_count[ambiguous_reference_name]=float(correct_contig_count[ambiguous_reference_name])+1
                                      else:
                                          correct_contig_count[ambiguous_reference_name]=1
                                          
                                      with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                          reference_file.write(strain+','+prediction+','+ambiguous_reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
                                          
                                      with open(output_directory+'/ambiguous_contigs.csv', 'a+') as ambiguous_contigs_file:
                                          ambiguous_contigs_file.write(strain+','+prediction+','+ambiguous_reference_name+','+contig_name+','+str(ambiguous_alignment_length)+','+start+','+end+'\n')
                                          
                                else: 
                                    alignment_fraction=round(int(ambiguous_alignment_length)/int(bin_length_dict[contig_name]),2)
                                    unaligned_length+=int(alignment_length)
                                    unaligned_count+=1
                                    low_qcov+=1
                                    with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                        reference_file.write(strain+','+prediction+','+ambiguous_reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')                                   
                                    with open(output_directory+'/unaligned_contigs.csv', 'a+') as unaligned_file:
                                        unaligned_file.write(strain+','+contig_name+','+str(bin_length_dict[contig_name])+','+'\n')
                            information_lines=[]
                            i+=1
                        
                        
                        
###------------------------------5. PROCESS DATA FOR CONTIGS THAT ARE MISASSEMBLED.
                        #MISASSEMBLED CONTIGS CAN BE TRANSLOCATIONS, RELOCATIONS OR INVERSIONS. SEE QUAST MANUAL.
                        #TRASLOCATIONS: A PART OF THE CONTIG ALIGNS TO ONE REPLICON, ANOTHER PART TO A DIFFERENT REPLICON.
                        elif 'misassembled' in line:    
                        #if we found a translocation information, we will split the alignment into the different replicons that it aligns to.
                            if any('translocation' in words for words in information_lines):
                                #We will loop over the lines that were accumulated in the variable information_lines.
                                total_information_lines=len(information_lines)
                                j=0
                                
                                while j < total_information_lines:
                                    alignment_information=information_lines[j].split('\t')
                                    #We will skip alignments classified as false.
                                    if any('False' in words for words in alignment_information): 
                                        j+=1
                                        
                                    else:
                                        #we will try to gather alignment information. But some lines will not contain this information, therefore we will skip them.
                                        try:
                                            #this section of the code will gather the information on the refrence replicon andof the contig name.
                                            reference_name=alignment_information[4]
                                            contig_name=alignment_information[5]
                                            alignment_length=abs(int(alignment_information[3])-int(alignment_information[2]))+1 #LENGTH OF THE ALIGNMENT
                                            indels=abs(alignment_length-(abs(int(alignment_information[0])-int(alignment_information[1]))+1))
                                            alignment_length=alignment_length-indels

                                            contig_fraction=round(int(alignment_length)/int(bin_length_dict[contig_name]),2) #same as alignment_fraction

                                            #gather alignment ranges for later evaluation of overlapping positions
                                            reference_positions=[int(alignment_information[0]),int(alignment_information[1])]
                                            start=str(min(reference_positions))
                                            end=str(max(reference_positions))
                                            min_alignnment_pos_dict[reference_name].append(start)
                                            max_alignnment_pos_dict[reference_name].append(end)  
                                            
                                            #get the contig alignment positions to each reference for later fix of atb-r assignment problem                                            
                                            reference_positions_contigs=[int(alignment_information[2]),int(alignment_information[3])]
                                            start_contig=str(min(reference_positions_contigs))
                                            end_contig=str(max(reference_positions_contigs))
                                            
                                            #In this cases we will not have the limitation of evaluating 90% coverage of the contig, since by definition this is a hybrid contig.                                                                                              
                                            if reference_name in correct_lengths:
                                                correct_lengths[reference_name]=int(correct_lengths[reference_name])+int(alignment_length)    
                                            else:
                                                correct_lengths[reference_name]=int(alignment_length)
                                            
                                            if reference_name in correct_contig_count:
                                                correct_contig_count[reference_name]=float(correct_contig_count[reference_name])+float(contig_fraction)    
                                            else:
                                                correct_contig_count[reference_name]=float(contig_fraction)  
                                            j+=1
                                            
                                            with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                                      reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(contig_fraction)+','+start_contig+','+end_contig+'\n')
                                                      
                                        #skipping lines that do not contain alignemtn information              
                                        except:
                                            j+=1
                                        
                                information_lines=[]
                                i+=1
                                
                            #in any other missasembly case, relocation or inversion, we will analyze the data as beofre, using the 90% filter for correct alignments 
                            else:
                                
                                #we will create a dictionary that will contain the total alignment lengths to different regions of the replicon. SO we can later make a sum.
                                temporary_alignment_dict={}
                                #we will create a dictionary that will contain the alignments ranges temporarly.
                                temporary_min_alignnment_pos_dict=defaultdict(list)
                                temporary_max_alignnment_pos_dict=defaultdict(list)
                                
                                total_information_lines=len(information_lines)
                                j=0
                                
                                #since there are probably multiple alignment lines in this cases, we will loop over the lines that were accumulated in the variable information_lines.
                                while j < total_information_lines:
                                    alignment_information=information_lines[j].split('\t')
                                    
                                    #skipping false alignments
                                    if any('False' in words for words in alignment_information):
                                        j+=1
                                        
                                    else:
                                        #we will try to gather alignment information. But some lines will not contain this information, therefore we will skip them.
                                        try:  
                                            #this section of the code will gather the information on the refrence replicon andof the contig name.                                                                                  
                                            reference_name=alignment_information[4]
                                            contig_name=alignment_information[5]
                                            alignment_length=abs(int(alignment_information[3])-int(alignment_information[2]))+1 #LENGTH OF THE ALIGNMENT
                                            indels=abs(alignment_length-(abs(int(alignment_information[0])-int(alignment_information[1]))+1))
                                            alignment_length=alignment_length-indels #real length
                                            
                                            contig_fraction=round(int(alignment_length)/int(bin_length_dict[contig_name]),2)
                                            
                                            #gather alignment ranges for later evaluation of overlapping positions
                                            reference_positions=[int(alignment_information[0]),int(alignment_information[1])]
                                            start=str(min(reference_positions))
                                            end=str(max(reference_positions))
                                            temporary_min_alignnment_pos_dict[reference_name].append(start)
                                            temporary_max_alignnment_pos_dict[reference_name].append(end)
                                            
                                            #we will add the lenght of alignment to the temporary alignment lengths.
                                            if reference_name in temporary_alignment_dict:
                                                temporary_alignment_dict[reference_name]=int(temporary_alignment_dict[reference_name])+int(alignment_length)
        
                                            else:
                                                temporary_alignment_dict[reference_name]=int(alignment_length)                                              
                                            j+=1
                                            
                                        except:
                                            j+=1
                              
                                #now we will check if the accumulated alignment lenghts are over 90% of the contig length.          
                                for reference_name in temporary_alignment_dict:
                                    if int(temporary_alignment_dict[reference_name]) >= int(bin_length_dict[contig_name])*0.9:
                                        alignment_fraction=round(int(temporary_alignment_dict[reference_name])/int(bin_length_dict[contig_name]),2)
                                        
                                        #Since the alignemtns are correct (more than 90%) we will add the temp_aligment_ranges.
                                        entry_numbers=len(temporary_min_alignnment_pos_dict[reference_name])
                                        k=0
                                        while k < entry_numbers:
                                            start=temporary_min_alignnment_pos_dict[reference_name][k]
                                            end=temporary_max_alignnment_pos_dict[reference_name][k]
                                            min_alignnment_pos_dict[reference_name].append(start)
                                            max_alignnment_pos_dict[reference_name].append(end)
                                            k+=1

                        #Add the length of the alignment to the dictionary correct_lengths. This will be use to calculate the purity percentage of the bin
                                        if reference_name in correct_lengths:
                                            correct_lengths[reference_name]=int(correct_lengths[reference_name])+int(temporary_alignment_dict[reference_name]) 
                                        else:
                                            correct_lengths[reference_name]=int(temporary_alignment_dict[reference_name])
                                              
                                        if reference_name in correct_contig_count:
                                            correct_contig_count[reference_name]=float(correct_contig_count[reference_name])+1    
                                        else:
                                            correct_contig_count[reference_name]=1                                      
                                                                      
                                        with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                            reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
                                        
                                    else: #If the accumulated length is not over 90%, we will skip the line.
                                        alignment_fraction=round(int(temporary_alignment_dict[reference_name])/int(bin_length_dict[contig_name]),2)
                                        unaligned_length+=int(alignment_length)
                                        unaligned_count+=1
                                        low_qcov+=1
                                        with open(output_directory+'/contigs_references.csv', 'a+') as reference_file:
                                            reference_file.write(strain+','+prediction+','+reference_name+','+contig_name+','+str(int(bin_length_dict[contig_name]))+','+str(alignment_fraction)+'\n')
        
                                i+=1
                                information_lines=[]
                                  
                                             
                        
                       #if no correct, or misassembled or ambiguous word is found, then we will save the line in the variable information_lines. 
                        else:
                            information_lines.append(line)
                            i+=1
                    
                            
###------------------------------6. ANALYZE OVERLAPPING REGIONS.
                    #This part of the script will calculate an overlapping lngth for each reference replicon. Which could later be substracted for calculating recall and precision.
                    
                    for reference_name in min_alignnment_pos_dict: #GO THRU EACH ENTRY OF THE ALIGNEMTN POSITION DICTIONARIES
                        number_of_alignments=len(min_alignnment_pos_dict[reference_name]) #GET THE LENGTH OF THIS DICTIONARY
                        k=1
                        while k<number_of_alignments:
                            l=0
                            while l<k:
                                if int(min_alignnment_pos_dict[reference_name][l])>=int(min_alignnment_pos_dict[reference_name][k]) and int(min_alignnment_pos_dict[reference_name][l])<=int(max_alignnment_pos_dict[reference_name][k]):
                                    if int(max_alignnment_pos_dict[reference_name][l]) >=int(max_alignnment_pos_dict[reference_name][k]):
                                        
                                        overlap_length= int(max_alignnment_pos_dict[reference_name][k])-int(min_alignnment_pos_dict[reference_name][l])+1
                                        if reference_name in overlapping:
                                            overlapping[reference_name]+=overlap_length
                                        else:
                                            overlapping[reference_name]=overlap_length
                                        
                                    else:
                                        overlap_length= int(max_alignnment_pos_dict[reference_name][l])-int(min_alignnment_pos_dict[reference_name][l])+1
                                        if reference_name in overlapping:
                                            overlapping[reference_name]+=overlap_length
                                        else:
                                            overlapping[reference_name]=overlap_length
                                            
                                    l+=1
                                
                                elif int(max_alignnment_pos_dict[reference_name][l])>int(min_alignnment_pos_dict[reference_name][k]) and int(max_alignnment_pos_dict[reference_name][l])<=int(max_alignnment_pos_dict[reference_name][k]):  

                                    if int(min_alignnment_pos_dict[reference_name][l]) >=int(min_alignnment_pos_dict[reference_name][k]):
                                        overlap_length= int(max_alignnment_pos_dict[reference_name][l])-int(min_alignnment_pos_dict[reference_name][l])+1
                                        if reference_name in overlapping:
                                            overlapping[reference_name]+=overlap_length
                                        else:
                                            overlapping[reference_name]=overlap_length
                                            
                                    else:
                                        overlap_length= int(max_alignnment_pos_dict[reference_name][l])-int(min_alignnment_pos_dict[reference_name][k])+1
                                        if reference_name in overlapping:
                                            overlapping[reference_name]+=overlap_length
                                        else:
                                            overlapping[reference_name]=overlap_length
                                            
                                    l+=1
                                 
                                else:
                                    l+=1                                     
                            k+=1
                            
###------------------------------7. SAVE ALL RESULTS OBTAINED FOR A SINGLE BIN.
                    
                    ##7.1 OVERLAPPING REGIONS  
                    for reference_name in overlapping:
                        with open(output_directory+'/overlapping_length.csv', 'a+') as overlapping_file:
                            overlapping_file.write(strain+','+prediction+','+reference_name+','+str(overlapping[reference_name])+'\n')
                    
                    ##7.2 COUNT OF AMBIGUOUS CONTIGS.
                    with open(output_directory+'/ambiguous_contigs_count.csv', 'a+') as ambiguous_count_file:
                        ambiguous_count_file.write(strain+','+prediction+','+str(ambiguous_count)+'\n')
                    
                    ##7.3 COUNT OF LOW-COVERAGE CONTIGS (LESS THAN 90% ALIGNEMTN COVERAGE)
                    with open(output_directory+'/low_coverage_contigs_count.csv', 'a+') as low_coverage_file:
                        low_coverage_file.write(strain+','+str(low_qcov)+'\n')
                        

                    ##7.4 FOR CORRECT ALIGNMENTS (IF ANY), PRINT IMPORTANT STATISTICS    
                    if len(correct_lengths)>0:
                    #Fill the percentages dictionary
                        for reference_name in correct_lengths:
                            if reference_name in overlapping: #remove overlappings
                                correct_length_percentages[reference_name]=(int(correct_lengths[reference_name])-int(overlapping[reference_name]))/(int(total_bin_lengths[prediction]))
                                correct_count_percentages[reference_name]=float(correct_contig_count[reference_name])/(sum(correct_contig_count.values())+unaligned_count)
                                with open(output_directory+'/alignment_statistics.csv', 'a+') as statistics_file:
                                     statistics_file.write(strain+','+prediction+','+reference_name+','+str(int(correct_lengths[reference_name])-int(overlapping[reference_name]))+','+str((total_bin_lengths[prediction]))+'\n')
                            else:
                                correct_length_percentages[reference_name]=int(correct_lengths[reference_name])/(total_bin_lengths[prediction])
                                correct_count_percentages[reference_name]=float(correct_contig_count[reference_name])/(sum(correct_contig_count.values())+unaligned_count)
                                with open(output_directory+'/alignment_statistics.csv', 'a+') as statistics_file:
                                     statistics_file.write(strain+','+prediction+','+reference_name+','+str(correct_lengths[reference_name])+','+str((total_bin_lengths[prediction]))+'\n')                                  

                    else:
                        with open(output_directory+'/alignment_statistics.csv', 'a+') as statistics_file:
                            statistics_file.write(strain+','+prediction+','+'no_correct_alignments'+','+'0'+','+'0'+'\n')                            
                                
                    
                   
            except FileNotFoundError:
                print('no_alignment_file')
                with open(output_directory+'/alignment_statistics.csv', 'a+') as statistics_file:
                         statistics_file.write(strain+','+prediction+','+'contig_length_below_1k'+','+'0'+','+'0'+'\n')                
    except FileNotFoundError:
        os.chdir(wd)
        
                        
    os.chdir(wd)

###------------------------------8. SET DIRECTORIES                                           

#directories paths
wd=os.path.dirname(os.path.realpath(__file__)) #<-SET INITIAL WD TO THE SCRIPT DIRECTORY
predictions_directory=sys.argv[1] #<-SET PREDICTIONS DIRECTORY AS FIRST COMMAND LINE ARGUMENT
alignment_directory=sys.argv[2] #<- SET DIRECTORY THAT CONTAINS THE RESULTS OF QUAST ALIGNMENTS
output_directory=sys.argv[3]

output_directory=output_directory.rstrip("/")

if not os.path.exists(output_directory):
    os.mkdir(output_directory)

#####---9.GET A LIST OF BINS TO PROCESS
os.chdir(predictions_directory)
#Now we will create a set of strain names
all_bins=glob.glob('GCA*fasta') #this will create a list with all bins (rather than all strains)

####--10. GET THE NAME OF THE STRAINS FROM THE PREDICTIONS

genomes=set()
for predictions in all_bins:
    strain_name=predictions.split('_')[0:3]
    strain_name=('_').join(strain_name)
    genomes.add(strain_name)

os.chdir(wd)

####--11. RUN SCRIPTS

for files in genomes:
    create_bin_length_dict(files)
    bin_status(files)
    
