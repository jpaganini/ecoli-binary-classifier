#!/bin/bash

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate abricate_mmbioit

#move to the gplas_plasmidec directory
cd ../gplas/results/final_benchmark/plasmidec_1kb/
#get the paths to the bins
find $(pwd) -type f | grep .fasta | grep -v installation | grep -v normal_mode >> ../../../../data/all_bins_path_new.txt

cd ../plascope
find $(pwd) -type f | grep .fasta | grep -v installation | grep -v normal_mode >> ../../../../data/all_bins_path_new.txt

#move to the mob-suite directory
cd ../../../../results/mob_predictions_1kb

#get the paths to the predictions
find $(pwd) -type f | grep .fasta | grep plasmid_ >> ../../data/all_bins_path_new.txt

##-----------------. RUN ABRICATE TO GET THE ATB-R genes in the bins -------------------------##
cd ../
#Run abricate
abricate --fofn ../data/all_bins_path_new.txt > all_bins_abricate_output.csv

