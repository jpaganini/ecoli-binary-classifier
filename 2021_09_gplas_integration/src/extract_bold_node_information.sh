#!/bin/bash

#1 Move to the directory
cd ../gplas/results

#2. get a list of files
files=$(ls *_results.tab | sed 's/_results.tab//g')

#3. loop thru the files and extract relevant information into a single file
for strain in $files
do
tail -n +2 ${strain}_results.tab | sed "s/$/ ${strain}/" >> ../../results/gplas_bold_nodes.tab
done
