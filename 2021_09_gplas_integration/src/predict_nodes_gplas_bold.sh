#!/bin/bash

source ~/data/miniconda3/etc/profile.d/conda.sh

#activate proper conda env
conda activate gplas_plasmidec_test

#get a list of strains
strains=$(cat ../data/unbinned_training.csv)

#move to gplas directory
cd ../gplas

#------Run with a bold_sd_coverage of 5.
for genome in $strains
do
./gplas.sh -i ../data/benchmark_ecoli_graphs/${genome}.gfa -c predict -s 'Escherichia coli' -x 50 -n ${genome} -b 5
done

#move the results
mkdir results/testing_bold_modes
mkdir results/testing_bold_modes/plasmidec_bold_05
mv results/GCA* results/testing_bold_modes/plasmidec_bold_05

#------Run with a bold_sd_coverage of 10.
for genome in $strains
do
./gplas.sh -i ../data/benchmark_ecoli_graphs/${genome}.gfa -c predict -s 'Escherichia coli' -x 50 -n ${genome} -b 10
done

#move the results
mkdir results/testing_bold_modes/plasmidec_bold_10
mv results/GCA*	results/testing_bold_modes/plasmidec_bold_10


#------Run with a bold_sd_coverage of 15.
for genome in $strains
do
./gplas.sh -i ../data/benchmark_ecoli_graphs/${genome}.gfa -c predict -s 'Escherichia coli' -x 50 -n ${genome} -b 15
done

#move the results
mkdir results/testing_bold_modes/plasmidec_bold_15
mv results/GCA*	results/testing_bold_modes/plasmidec_bold_15

#------Run with a bold_sd_coverage of 20.
for genome in $strains
do
./gplas.sh -i ../data/benchmark_ecoli_graphs/${genome}.gfa -c predict -s 'Escherichia coli' -x 50 -n ${genome} -b 20
done

#move the results
mkdir results/testing_bold_modes/plasmidec_bold_20
mv results/GCA*	results/testing_bold_modes/plasmidec_bold_20

