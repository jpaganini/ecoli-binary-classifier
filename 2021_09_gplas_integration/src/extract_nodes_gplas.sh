#!/bin/bash

source ~/data/miniconda3/etc/profile.d/conda.sh

#activate proper conda env
conda activate gplas_plasmidec_test

#get a list of strains
strains=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#move to gplas directory
cd ../gplas

#loop thru the graphs
for genome in $strains
do
./gplas.sh -i ../../../2021_08_prepare_dataset/results/benchmark_ecoli_graphs/${genome}.gfa -c extract -n ${genome}
done
