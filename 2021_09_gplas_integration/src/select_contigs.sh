#!/bin/bash

#activate conda env
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate seqtk_mmbioit

#Make directory for holding results 
mkdir ../plasmidEC/ecoli_nodes_1kb
mkdir ../plasmidEC/ecoli_nodes_500bp

#get a list of files
files=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

for isolate in $files
do
seqtk seq -L 1000 ../gplas/gplas_input/${isolate}_raw_nodes.fasta > ../plasmidEC/ecoli_nodes_1kb/${isolate}.fasta
seqtk seq -L 500 ../gplas/gplas_input/${isolate}_raw_nodes.fasta > ../plasmidEC/ecoli_nodes_500bp/${isolate}.fasta
done

