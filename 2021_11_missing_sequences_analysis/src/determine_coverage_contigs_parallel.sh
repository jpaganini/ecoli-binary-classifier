#!/bin/bash

#1. mkdir for holding the scripts
mkdir coverage_jobs_contigs

#2. mkdir for results
mkdir ../results/read_coverage_contigs

#3. Get a list of strains
list_strains=$(cat ../results/missing_plasmids.csv)

#4. Create files
for strains in $list_strains
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate bedtools_mmbioit
bedtools genomecov -d -ibam ../../results/bam_files_sorted_contigs/${strains}_sorted.bam > ../../results/read_coverage_contigs/${strains}.cov" > coverage_jobs_contigs/${strains}.sh
done

#5. Run the scripts
cd coverage_jobs_contigs
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=4:00:00 --mem=8G ${slurm} -c 8
done
