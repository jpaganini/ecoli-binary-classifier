#!/bin/bash

#1. Get a list of all benchmark genomes
list_strains=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#2. Create a directory for bwa jobs
mkdir bwa_jobs

#3. create directory for holding sam files
mkdir ../results/sam_files

#4. Index reference genomes and align reads
for strains in $list_strains
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate bwa_mmbioit
bwa index ../../../2021_08_prepare_dataset/data/reference_genomes/${strains}_genomic.fna
bwa mem ../../../2021_08_prepare_dataset/data/reference_genomes/${strains}_genomic.fna ../../../2021_08_prepare_dataset/data/trimmed_reads/${strains}_R1.fq ../../../2021_08_prepare_dataset/data/trimmed_reads/${strains}_R2.fq > ../../results/sam_files/${strains}.sam" > bwa_jobs/${strains}.sh
done

#5. Run the scripts
cd bwa_jobs
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:00:00 --mem=8G ${slurm} -c 8
done



