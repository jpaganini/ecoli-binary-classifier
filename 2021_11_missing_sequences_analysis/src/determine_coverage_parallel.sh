#!/bin/bash

#1. mkdir for holding the scripts
mkdir coverage_jobs

#2. mkdir for results
mkdir ../results/read_coverage

#3. Get a list of strains
list_strains=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#4. Create files
for strains in $list_strains
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate bedtools_mmbioit
bedtools genomecov -d -ibam ../../results/bam_files_sorted/${strains}_sorted.bam > ../../results/read_coverage/${strains}.cov" > coverage_jobs/${strains}.sh
done

#5. Run the scripts
cd coverage_jobs
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=4:00:00 --mem=8G ${slurm} -c 8
done
