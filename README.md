An optimised short-read approach for predicting and reconstructing ARG-plasmids of *E. coli*
================
Julian A. Paganini

Lisa C. Vader

## Code and data availability

Complete code and files used to generate the analysis reported in our
manuscript are fully available and can be run in Rstudio:

**1- Benchmarking binary classification tools**

To reproduce Figures 2 and 3, Supplementary Figures S1 and S4 and Supplementary Tables S1 and S2, please run the following script:

\[<https://gitlab.com/jpaganini/ecoli-binary-classifier/-/blob/master/2021_05_binary_classifier_benchmark/benchmarking/analysis/scripts/compare_binary_classifiers.Rmd>\].

**2- Benchmarking gplas2**

To reproduce Figure 4, Supplementary Figures S2, S3, S5, S6, S7, S10 and Table 1, please run the following script:

\[<https://gitlab.com/jpaganini/ecoli-binary-classifier/-/blob/master/2021_09_gplas_integration/src/benchmarking_gplas_2.rmd>\]

**3- Missing plasmids**

To reproduce Supplementary Figures S8 and S9, please run the following script:
\[<https://gitlab.com/jpaganini/ecoli-binary-classifier/-/blob/master/2021_11_missing_sequences_analysis/src/check_missing_plasmids.Rmd>\]

*Important. In order to run this script and reproduce figures S8 and S9, is necessary to download files from the following Zenodo link:*

https://zenodo.org/record/8182642

*Files should be placed in:*

*2021_11_missing_sequences_analysis/results/read_coverage* 

*2021_11_missing_sequences_analysis/results/read_coverage_contig*


## Issues/Questions

Feel free to contact me via email if you have any questions or suggestions
for improvement <j.a.paganini@umcutrecht.nl>
