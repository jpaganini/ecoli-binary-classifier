#!/bin/bash

#make results directory
mkdir -p ../results/centrifuge_predictions

#download E. coli database
mkdir -p ../databases/centrifuge
cd ../databases/centrifuge
if [[ ! -f chromosome_plasmid_db.3.cf ]]; then
	wget https://zenodo.org/record/1311641/files/chromosome_plasmid_db.tar.gz
	tar -xzf chromosome_plasmid_db.tar.gz
	rm chromosome_plasmid_db.tar.gz
fi

run_centrifuge(){
source /hpc/dla_mm/lvader/data/miniconda3/etc/profile.d/conda.sh
conda activate centrifuge
cd ../results/centrifuge_predictions
#check whether input directory exists
-[ ! -d ../../$1 ] && exit 1
#run centrifuge on all strains in input directory
for strain in ../../$1/*.fasta
do
name=$(basename $strain .fasta)
centrifuge -f --threads 8 -x ../../database/centrifuge/chromosome_plasmid_db -U ../../$1/${name}.fasta -k 1 --report-file ./${name}_summary -S ./${name}_extendedresults
done
}

run_centrifuge_postprocessing(){
source /hpc/dla_mm/lvader/data/miniconda3/etc/profile.d/conda.sh
conda activate R_codes
for strain in ../results/centrifuge_predictions/*extendedresults
do
name=$(basename $strain _extendedresults)
Rscript transform_centrifuge_output.R ../../results/centrifuge_predictions $name
done
}

while getopts :i: flag; do
	case $flag in
		i) path=$OPTARG;;
	esac
done

#check if input is present
[ -z $path ] && exit 1

#activate conda and run centrifuge
run_centrifuge $path
run_centrifuge_postprocessing $path

